import axios, {
  AxiosError,
  AxiosInstance,
  AxiosResponse,
  InternalAxiosRequestConfig,
} from "axios";
import { getBaseUrl } from "./baseUrl";
import { useErrorsStore } from "@/store/errors";
import { useUserStore } from "@/store/user";
import { useNotificationStore } from "@/store/notification";
import Cookies from "js-cookie";

export const axiosInstance: AxiosInstance = axios.create({
  baseURL: getBaseUrl(),
});

axiosInstance.interceptors.request.use((config: InternalAxiosRequestConfig) => {
  const userStore = useUserStore();
  const errorStore = useErrorsStore();

  if (userStore.token) {
    config.headers["Authorization"] = `${"Bearer " + userStore.token}`;
  }
  config.headers["Accept-Language"] = Cookies.get("locale") || "ar";
  if (
    errorStore.serverErrors &&
    Object.keys(errorStore.serverErrors).length > 0
  ) {
    errorStore.serverErrors = null;
  }
  return config;
});

axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => {
    const { method } = response.config;
    const notificationStore = useNotificationStore();

    if (method === "post" || method === "put" || method === "delete") {
      notificationStore.showNotification({
        color: "success",
        message: response.data.message,
      });
    }
    return response;
  },
  (error: AxiosError) => {
    const errorStore = useErrorsStore();
    const userStore = useUserStore();
    const notificationStore = useNotificationStore();

    const status = error.response?.status;
    const data = error.response?.data as { message: string; errors: any };
    if (status === 401 || status === 403) {
      userStore.logout();
    }
    notificationStore.showNotification({
      color: "error",
      message: data?.message,
    });

    if (data && data.errors) {
      errorStore.serverErrors = data.errors;
    }

    return Promise.reject(error);
  }
);
