import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import { defineAsyncComponent } from 'vue';
const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: defineAsyncComponent(() => import('@/views/index.vue')),
  },

  {
    path: "/sections",
    component: defineAsyncComponent(() => import("@/views/Sections/index.vue")),
  },
  {
    path: "/latest-stores",
    component: defineAsyncComponent(() => import("@/views/LatestStores/index.vue")),
  },
  {
    path: "/presenters/:store_id/details/:presenter_id",
    component: defineAsyncComponent(() => import("@/views/Presenters/PresenterDetails/index.vue")),
  },
  {
    path: "/presenters/:id",
    component: defineAsyncComponent(() => import("@/views/Presenters/index.vue")),
  },
  {
    path: "/subscription",
    component: defineAsyncComponent(() => import("@/views/Subscription/index.vue")),
  },
  {
    path: "/subscription/complete/:id",
    component: defineAsyncComponent(() => import(
      "@/views/Subscription/CompleteSubscription/index.vue"
    )),
  },
  {
    path: "/subscription-successfully",
    component: defineAsyncComponent(() => import("@/views/Subscription/SubscriptionMessage/index.vue")),
  },
  {
    path: "/subscription-packages/:id",
    component: defineAsyncComponent(() => import(
      "@/views/Subscription/SubscriptionPackages/index.vue"
    )),
  },
  {
    path: "/common-questions",
    component: defineAsyncComponent(() => import("@/views/CommonQuestions/index.vue")),
  },

  {
    path: "/contact-us",
    component: defineAsyncComponent(() => import("@/views/ContactUs/index.vue")),
  },
  {
    path: "/privacy-policy",
    component: defineAsyncComponent(() => import("@/views/PrivacyPolicy/index.vue")),
  },
  {
    path: "/condition-replacement",
    component: defineAsyncComponent(() => import("@/views/ConditionForReplacment/index.vue")),
  },
  {
    path: "/about-us",
    component: defineAsyncComponent(() => import("@/views/AboutUs/index.vue")),
  },
  {
    path: "/:pathMatch(.*)*",
    component: defineAsyncComponent(() => import("@/views/NotFound.vue")),
  },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((from, to, next) => {
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
  next();
});

export default router;
