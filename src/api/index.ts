// singleton class
import { axiosInstance } from "@/configs/axios";
import { HttpMethods, HttpType } from "./types";
import { provide } from "vue";

class Http implements HttpMethods {
  static instance: {};

  constructor() {
    if (Http.instance instanceof Http) {
      return Http.instance;
    }
    Http.instance = Object.freeze(this);
  }

  get<T>({ url, query }: HttpType): Promise<T> {
    return axiosInstance({
      url,
      params: { ...query },
      method: "get",
    });
  }
  post<T>({ url, data }: HttpType): Promise<T> {
    return axiosInstance({
      url,
      data,
      method: "post",
    });
  }
}

export const $http: HttpMethods = new Http();
