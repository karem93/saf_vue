export interface HttpType {
  url: string;
  query?: { [key: string]: any };
  data?: { [key: string]: any };

  [key: string]: any;
}

export interface HttpMethods {
  get: <T extends unknown>(props: HttpType) => Promise<T>;
  post: <T extends unknown>(props: HttpType) => Promise<T>;
}
