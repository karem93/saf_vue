import App from "./App.vue";

import { createApp } from "vue";

// Plugins
import { registerPlugins } from "@/plugins";
import { registerGlobal } from "./plugins/components";
import { i18n } from "./lang";
import "./bootstrap";

export const app = createApp(App);

registerGlobal(app);
registerPlugins(app);
app.use(i18n);
app.mount("#app");
