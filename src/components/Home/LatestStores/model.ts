export interface Props<T> {
  fromStore?: boolean;
  loadingData?: boolean;
  items?: T[];
}
