import { type } from "os";

interface IImage {
  img: string;
}

type TStatus = "not_exists" | "liked" | "dislike";

export interface IProductDetail {
  bio: string;
  cover_img: string;
  discount: number;
  dislikes: number;
  file: string;
  flag: number;
  id: number;
  images: IImage[];
  is_liked: TStatus;
  likes: number;
  name: string;
  price: number;
  price_after: number;
  views: number;
}
