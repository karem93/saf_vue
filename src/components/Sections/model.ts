import { Country } from "@/store/app";

export interface SectionItem {
  id: number;
  name: string;
  image: string;
  country: Country;
}
