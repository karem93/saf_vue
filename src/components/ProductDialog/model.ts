export type TStatus = "not_exists" | "liked" | "dislike";
export type StatusLoading = {
  [key in TStatus]: boolean;
};
