interface Item {
  title: string;
  disabled: boolean;
  to: string;
  exact: boolean;
}

export interface Props {
  items: Item[];
}
