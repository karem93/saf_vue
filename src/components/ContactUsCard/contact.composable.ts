import { toTypedSchema } from "@vee-validate/yup";
import { useForm } from "vee-validate";
import { ref } from "vue";
import { useI18n } from "vue-i18n";

import { object, string } from "yup";
import { provide } from "vue";
import { $http } from "@/api";
const loading = ref<boolean>(false);
export const useContactFormComposable = () => {
  const { t } = useI18n();
  const { handleSubmit, resetForm } = useForm({
    validationSchema: toTypedSchema(
      object({
        full_name: string().required(t("validations.required")),
        message_type: string().required(t("validations.required")),
        email: string()
          .required(t("validations.required"))
          .email(t("validations.invalid_email")),
        content: string().required(t("validations.required")),
      })
    ),
    initialValues: {
      full_name: "",
      message_type: "",
      email: "",
      content: "",
    },
    validateOnMount: true,
  });

  const selectItems: { name: string; value: string }[] = [
    { name: t("suggestion"), value: "suggestion" },
    { name: t("inquiry"), value: "inquiry" },
    { name: t("complaint"), value: "complaint" },
  ];

  const onSubmit = handleSubmit(async (formValue) => {
    try {
      loading.value = true;
      await $http.post({ url: "contactUs", data: formValue });
      loading.value = false;
      resetForm();
    } finally {
      loading.value = false;
    }
  });

  return {
    onSubmit,
    loading,
    provide,
    selectItems,
  };
};
