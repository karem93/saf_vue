import { defineStore } from "pinia";
import { $http } from "@/api";

interface Base {
  id: number;
  img: string;
}
export interface Slider extends Base {
  link: string;
}
export interface Section extends Base {
  name: string;
}
export interface Country {
  id: number;
  name: string;
  flag: string;
  phone_code: string;
}
export interface Product {
  bio: string;
  cover_img: string;
  discount: number;
  flag: number;
  id: number;
  name: string;
  price: number;
  price_after: number;
}

export interface StoreType {
  id: number;
  name: string;
  image: string;
  country: Country;
}

interface Review {
  img: string;
  id: number;
  name: string;
  job_name: string;
  message: string;
  created_at: string;
}
interface StaticPage {
  content: string;
  id: 1;
  image: string;
  title: string;
}

interface ContactInfo {
  address: string;
  contact_email: string;
  phone: string;
  facebook_link: string;
  twitter_link: string;
  snapchat_link: string;
  instagram_link: string;
  youtube_link: string;
  latitude: string;
  longitude: string;
}
interface HomeData {
  sliders: Slider[];
  sections: Section[];
  stores: StoreType[];
  reviews: Review[];
  staticpage: StaticPage;
}

export const useAppStore = defineStore("app", {
  state: () => ({
    homeData: {} as HomeData,
    contact_info: {} as ContactInfo,
    loadingData: true,
  }),
  actions: {
    async fetchHomeData(url: string = "homepage") {
      const res = await $http.get<{ data: { data: HomeData } }>({
        url,
      });
      const { data } = res.data;
      this.homeData = data;
      this.loadingData = false;
    },
    async fetchContactInfo() {
      const res = await $http.get<{ data: { data: ContactInfo[] } }>({
        url: "contactInfo",
      });
      const { data } = res.data;
      this.contact_info = data.reduce(
        (acc: ContactInfo | any, value: ContactInfo) => {
          const objValue: any = Object.fromEntries(Object.entries(value));
          return { ...acc, ...objValue };
        },
        {}
      );
    },
  },
  getters: {
    getHomeData(state) {
      return state.homeData;
    },
    getHomeStores({ homeData }) {
      return homeData?.stores?.slice(0, 5);
    },
    getHomeSliders({ homeData }): Slider[] {
      return homeData?.sliders ? homeData?.sliders : [];
    },
    getHomeReviews({ homeData }): Review[] {
      return homeData?.reviews ? homeData?.reviews : [];
    },
    getHomeStaticPage({ homeData }): StaticPage {
      return homeData?.staticpage;
    },
    getContactInfo({ contact_info }) {
      return contact_info;
    },
  },
});
