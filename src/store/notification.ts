// Utilities
import { defineStore } from "pinia";

type Color = "error" | "success";
export const useNotificationStore = defineStore("notification", {
  state: () => ({
    open: false,
    message: null as string | null,
    color: "success" as Color,
  }),
  actions: {
    showNotification({ color, message }: { color: Color; message: string }) {
      this.open = true;
      this.message = message;
      this.color = color;
    },
  },
});
