// Utilities
import { defineStore } from "pinia";

export const useErrorsStore = defineStore("errors", {
  state: () => ({
    serverErrors: null,
  }),
  actions: {
    setServerErrors() {},
  },
});
