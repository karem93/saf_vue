import { defineStore } from "pinia";
import { $http } from "@/api";
import cookie from "js-cookie";
import router from "@/router";

interface UserProps {
  name: string;
  user_type: string;
  [key: string]: any;
}

export const useUserStore = defineStore("user", {
  state: () => ({
    token: cookie.get("token") || null,
    user: {} as UserProps | any,
    contact_us: null,
    zone_id: null,
    domain_status: null,
    domains: [] as { domain: string }[],
    admins: ["super_admin", "admin"],
  }),
  actions: {
    async login(data: { username: string; password: string }) {
      // return await $http.post({ url: "login", data }).then((res) => {
      //   const { data } = res.data;
      //   this.token = data["_token"];
      //   this.user = data;
      //   cookie.set("token", data["_token"]);
      //   if (this.admins.includes(data.user_type)) {
      //     router.push("/dashboard");
      //   } else {
      //     this.getUser().then((id) => {
      //       if (this.domain_status && this.domain_status === "pending") {
      //         router.push(`/domains/${id}`);
      //       } else {
      //         router.push(`/domain/${id}`);
      //       }
      //     });
      //   }
      // });
    },
    logout() {
      this.token = null;
      this.user = null;
      cookie.remove("token");
      router.push("/login");
    },
    async getUser() {
      let url: string;

      if (
        window.location.href.includes("dashboard") ||
        this.admins.includes(this.user.user_type)
      ) {
        url = "/dashboard/profile";
      } else {
        url = "profile";
      }

      return await $http.get({ url }).then((res) => {});
    },
    async getContactUs() {
      return await $http.get<any>({ url: "contacts" }).then((res) => {
        const { data } = res.data;
        this.contact_us = data.whatsapp;
      });
    },
    async register(data: FormData) {
      return await $http.post({ url: "register", data });
    },
  },
});
