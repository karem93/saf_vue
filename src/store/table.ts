// Utilities
import { defineStore } from "pinia";
import { $http } from "@/api";
import { useQueryStore } from "./query";
import { useRoute } from "vue-router";

interface TableItemType {
  url: string;
  query?: { [key: string]: any };
  param?: number | string;
}
interface Pagination {
  last_page: number;
  per_page: number;
  current_page: number;
}

const default_pagination: Pagination = {
  current_page: 0,
  last_page: 0,
  per_page: 0,
};
export const useTableStore = defineStore("table", {
  state: () => ({
    items: [],
    loading: true as boolean,
    pagination: {
      ...default_pagination,
    } as Pagination,
  }),
  actions: {
    setPagination(payload: Pagination) {
      this.pagination = payload;
    },
    // async getTableItems({ url }: TableItemType) {
    //   this.loading = true;
    //   const queryStore = useQueryStore();
    //   const res = await $http.get({
    //     url: `${url}`,
    //     query: queryStore.query,
    //   });
    //   const { data, meta } = res.data;
    //   if (meta) {
    //     this.pagination = meta;
    //   } else {
    //     this.pagination = { ...default_pagination };
    //   }
    //   this.items = data;
    //   this.loading = false;
    // },
    // async deleteTableItem({ url, param }: TableItemType) {
    //   await $http.post({
    //     url: `${url}/${param}`,
    //     data: { _method: "delete" },
    //   });
    //   this.getTableItems({ url });
    // },
  },
});
