// Utilities
import { defineStore } from "pinia";
import { $http } from "@/api";

interface TableItemType {
  url: string;
  query?: { [key: string]: any };
}
interface Pagination {
  last_page: number;
  per_page: number;
  current_page: number;
}
export const useQueryStore = defineStore("query", {
  state: () => ({
    query: {} as { [key: string]: any },
  }),
  actions: {
    setQuery(qs: { [key: string]: any }) {
      this.query = { ...this.query, ...qs };
    },
    clearFilter() {
      this.query = {};
    },
    removeFilter(filterName: string) {
      delete this.query[filterName];
    },
  },
});
