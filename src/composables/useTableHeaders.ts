import { useI18n } from "vue-i18n";
export interface Header {
  title: string;
  key?: string;
  sortable?: boolean;
  icon?: string;
  [key: string]: any;
}
export const useTableHeaders = (headers: Header[]) => {
  const { t } = useI18n();
  return headers.map(({ title, key, ...props }: Header) => ({
    title: t(`${title}`),
    key,
    ...props,
  }));
};
