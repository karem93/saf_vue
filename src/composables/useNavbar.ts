import { useI18n } from "vue-i18n";

export interface NavBar {
  text: string;
  to: string;
  icon?: string;
}

export const useNavbar = (): NavBar[] => {
  const { t } = useI18n();

  return [
    { text: t("home"), to: "/" },
    { text: t("sections"), icon: "mdi-chevron-down", to: "/sections" },
    { text: t("common_questions"), to: "/common-questions" },
    { text: t("contact_us"), to: "/contact-us" },
  ];
};
