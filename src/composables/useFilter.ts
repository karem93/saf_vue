import { observer } from "@/helpers/observer";
import { useQueryStore } from "../store/query";
import { debounce } from "@/helpers/debounce";

export interface Props {
  filterName: string;
}

export const useFilter = ({ filterName }: Props) => {
  const store = useQueryStore();
  let query: { [key: string]: any } = {};
  // const handleFilter = debounce(() => {
  //   query = { [filterName]: form["value"][filterName] };
  //   if (form["value"][filterName]) {
  //     store.setQuery(query);
  //     observer.fire("getTableData");
  //   } else {
  //     removeFilter();
  //   }
  // });
  const handleFilter = () => {
    query = { [filterName]: store.query[filterName] };
    if (store.query[filterName]) {
      store.setQuery(query);
      // observer.fire("getTableData");
    } else {
      removeFilter();
    }
  };

  const removeFilter = () => {
    delete query[filterName];
    store.removeFilter(filterName);
    // observer.fire("getTableData");
  };

  const clearFilter = () => {
    store.clearFilter();
    observer.fire("getTableData");
  };

  return { handleFilter, removeFilter, clearFilter };
};
