export interface Section {
  id: number;
  name: string;
  img: string;
}
