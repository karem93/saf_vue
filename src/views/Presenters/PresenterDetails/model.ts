import { Product, Slider } from "@/store/app";
import { Presenter } from "../model";

export interface PresenterDetails {
  presenter: (Presenter & Social) | any;
  sliders: Slider[];
  products: Product[];
}

export interface Social {
  email: string;
  facebook: string;
  instagram: string;
  phone: string;
  twitter: string;
  whatsAPP: string;
  [key: string]: any;
}
