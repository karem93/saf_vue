import { Country } from "@/store/app";

export interface Presenter {
  name: string;
  id: number;
  company_img: string;
  country: Country;
  bio: string;
}
