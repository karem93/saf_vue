export interface IFaq {
  answer: string;
  id: number;
  is_active: number;
  question: string;
}
