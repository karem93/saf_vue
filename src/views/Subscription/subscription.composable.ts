import { toTypedSchema } from "@vee-validate/yup";
import { useForm } from "vee-validate";
import { ref } from "vue";
import { useI18n } from "vue-i18n";
import { useRoute, useRouter } from "vue-router";
import { array, number, object, string } from "yup";
import { defineAsyncComponent, provide } from "vue";
import { $http } from "@/api";
import { emailPattern } from "@/utils/emailValidationRegex";
import { phoneValidation } from "@/utils/phoneValidationRegex";
export const useSubscription = () => {
  const FirstStep = defineAsyncComponent(
    () => import("@/components/Subscription/FirstStep.vue")
  );
  const SecondStep = defineAsyncComponent(
    () => import("@/components/Subscription/SecondStep.vue")
  );

  const components = [FirstStep, SecondStep];

  const { t } = useI18n();
  const route = useRoute();
  const router = useRouter();
  const loading = ref<boolean>(false);
  const step = ref<number>(0);
  const items = [
    {
      title: t("home"),
      disabled: false,
      to: "/",
      exact: true,
    },
    {
      title: t("subscription"),
      disabled: true,
      to: route.fullPath,
      exact: true,
    },
  ];

  const { handleSubmit, values } = useForm({
    validationSchema: toTypedSchema(
      object({
        f_name: string().required(t("validations.required")).min(5, t('validations.min', { min: 5 })),
        l_name: string().required(t("validations.required")).min(5, t('validations.min', { min: 5 })),
        phone: string().matches(phoneValidation, t("validations.invalid_phone")).required(t("validations.required")),
        country: object({
          phone_code: string().required(t("validations.required")),
          id: string().required(t("validations.required")),
        }).required(t("validations.required")),

        type: string()
          .optional()
          .default("visitor")
          .oneOf(["visitor", "presenter"]),
        sections: array()
          .of(number().required(t("validations.required")))
          .required(t("validations.required")),
        email: string().matches(emailPattern, t("validations.invalid_email"))
          .email(t("validations.invalid_email"))

          .required(t("validations.required")),
      })
    ),
    initialValues: {
      f_name: "",
      l_name: "",
      email: "",
      country: undefined,
      phone: undefined,
      sections: undefined,
      type: "visitor",
    },
  });

  const generateFormData = () => {
    const formData = new FormData();
    const { country, sections, ...resetData }: { [key: string]: any } = values;
    for (const key in resetData) {
      formData.append(key, resetData[key]);
    }

    formData.append("phone_code", country.phone_code);
    formData.append("country_id", country.id);

    (sections as []).forEach((id, index) => {
      formData.append(`sections[${index}]`, id);
    });

    return formData;
  };

  const onSubmit = async () => {
    try {
      loading.value = true;
      const res = await $http.post<{ data: { data: { id: number } } }>({
        url: "addUser",
        data: generateFormData(),
      });
      const { data } = res.data;
      if (values.type == "presenter") {
        router.push(`/subscription/complete/${data.id}`);
      } else {
        router.push("/subscription-successfully");
      }
      loading.value = false;
    } finally {
      loading.value = false;
    }
  };

  const handleNext = handleSubmit(() => {
    if (components.length - 1 == step.value) return;
    step.value++;
  });

  const onChange = (type: string) => {
    values.type = type;
  };

  provide("handleNext", handleNext);
  provide("onSubmit", onSubmit);
  provide("onChange", onChange);

  return {
    step,
    loading,
    items,
    components,
    provide,
  };
};
