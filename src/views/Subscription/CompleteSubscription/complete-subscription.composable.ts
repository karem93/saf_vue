import { toTypedSchema } from "@vee-validate/yup";
import { useForm } from "vee-validate";
import { ref } from "vue";
import { useI18n } from "vue-i18n";
import { useRoute } from "vue-router";
import { mixed, object, string } from "yup";
import { $http } from "@/api";
export const useCompleteSubscription = () => {
  const { t } = useI18n();
  const route = useRoute();
  const loading = ref<boolean>(false);
  const items = [
    {
      title: t("home"),
      disabled: false,
      to: "/",
      exact: true,
    },
    {
      title: t("subscribe_as_exhibitor"),
      disabled: false,
      to: "/subscription",
      exact: true,
    },
    {
      title: t("complete_data"),
      disabled: true,
      to: route.fullPath,
      exact: true,
    },
  ];

  const { handleSubmit, values } = useForm({
    validationSchema: toTypedSchema(
      object({
        company_name: string().required(t("validations.required")),
        tax_record: string().required(t("validations.required")),
        company_image: mixed().required(t("validations.required")),
        commericalRegister_img: mixed().required(t("validations.required")),
        user_id: string().optional(),
      })
    ),
    initialValues: {
      company_name: "",
      tax_record: "",
      company_image: undefined,
      commericalRegister_img: undefined,
      user_id: undefined,
    } as { [key: string]: any },
  });

  const generateFormData = () => {
    const formData = new FormData();
    for (const key in values) {
      formData.append(key, values[key]);
    }
    formData.append("user_id", route.params["id"] as string);
    return formData;
  };

  const handleCompleteSubscription = async () => {
    try {
      await $http.post({ url: "addPresenter", data: generateFormData() });
    } finally {
      loading.value = false;
    }
  };

  const onSubmit = handleSubmit(() => {
    loading.value = true;
    handleCompleteSubscription();
  });

  return {
    onSubmit,
    values,
    loading,
    items,
  };
};
