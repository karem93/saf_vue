export interface ListStore {
  id: number;
  img: string;
  name: string;
}

export interface Pagination {
  current_page: number;
  last_page: number;
  per_page: number;
}
