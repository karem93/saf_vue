// Types
import type { App } from "vue";

import TextField from "@/components/form/TextField.vue";
import SelectField from "@/components/form/SelectField.vue";
import Autocomplete from "@/components/form/Autocomplete.vue";
import FileUpload from "@/components/form/FileUpload.vue";
import TextArea from "@/components/form/TextArea.vue";
import Notification from "@/components/Notification/index.vue";
import Loader from "@/components/Loader/index.vue";
import Pagination from "@/components/Pagination/index.vue";
import CustomButton from "@/components/CustomButton/index.vue";
import GenericDialog from "@/components/GenericDialog/index.vue";
import SectionTitle from "@/components/SectionTitle/index.vue";
import PageTitle from "@/components/PageTitle/index.vue";
import Breadcrumbs from "@/components/Breadcrumbs/index.vue";
import Skeleton from "@/components/Skeleton/index.vue";
import CategoryCard from "@/components/Cards/CategoryCard.vue";
import WrapperCard from "@/components/Cards/index.vue";

//filters
import SearchFilter from "@/components/Filters/SearchFilter.vue";
import SelectFilter from "@/components/Filters/SelectFilter.vue";
import { $http } from "@/api";

export function registerGlobal(app: App) {
  app.component("text-field", TextField);
  app.component("Loader", Loader);
  app.component("file-upload", FileUpload);
  app.component("TextArea", TextArea);
  app.component("select-field", SelectField);
  app.component("auto-complete", Autocomplete);
  app.component("Notification", Notification);
  app.component("Pagination", Pagination);
  app.component("CustomButton", CustomButton);
  app.component("search-filter", SearchFilter);
  app.component("select-filter", SelectFilter);
  app.component("GenericDialog", GenericDialog);
  app.component("section-title", SectionTitle);
  app.component("page-title", PageTitle);
  app.component("breadcrumbs", Breadcrumbs);
  app.component("category-card", CategoryCard);
  app.component("wrapper-card", WrapperCard);
  app.component("skeleton-loader", Skeleton);

  app.provide("$http", $http);
}
