import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";
import * as labsComponents from "vuetify/labs/components";

import { ar, en } from "vuetify/locale";

import { createVuetify } from "vuetify";
import Cookies from "js-cookie";
import colors from "@/assets/scss/color_theme.module.scss";
type Locale = "ar" | "en";
const locale = (
  !Cookies.get("locale") ? "ar" : Cookies.get("locale")
) as Locale;
export default createVuetify({
  components: {
    ...labsComponents,
  },

  locale: {
    locale,
    fallback: "ar",
    messages: { ar, en },
    rtl: { ar: locale === "ar" ? true : false },
  },
  theme: {
    themes: {
      light: {
        colors,
      },
    },
  },
});
